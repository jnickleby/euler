'''
The four adjacent digits in the 1000-digit number that have the greatest product are 9 × 9 × 8 × 9 = 5832.

73167176531330624919225119674426574742355349194934
96983520312774506326239578318016984801869478851843
85861560789112949495459501737958331952853208805511
12540698747158523863050715693290963295227443043557
66896648950445244523161731856403098711121722383113
62229893423380308135336276614282806444486645238749
30358907296290491560440772390713810515859307960866
70172427121883998797908792274921901699720888093776
65727333001053367881220235421809751254540594752243
52584907711670556013604839586446706324415722155397
53697817977846174064955149290862569321978468622482
83972241375657056057490261407972968652414535100474
82166370484403199890008895243450658541227588666881
16427171479924442928230863465674813919123162824586
17866458359124566529476545682848912883142607690042
24219022671055626321111109370544217506941658960408
07198403850962455444362981230987879927244284909188
84580156166097919133875499200524063689912560717606
05886116467109405077541002256983155200055935729725
71636269561882670428252483600823257530420752963450

Find the thirteen adjacent digits in the 1000-digit number that have the greatest product. What is the value of this product?
'''


# Variables

sample =  "73167176531330624919225119674426574742355349194934" 
sample += "96983520312774506326239578318016984801869478851843" 
sample += "85861560789112949495459501737958331952853208805511"
sample += "12540698747158523863050715693290963295227443043557"
sample += "66896648950445244523161731856403098711121722383113"
sample += "62229893423380308135336276614282806444486645238749"
sample += "30358907296290491560440772390713810515859307960866"
sample += "70172427121883998797908792274921901699720888093776"
sample += "65727333001053367881220235421809751254540594752243"
sample += "52584907711670556013604839586446706324415722155397"
sample += "53697817977846174064955149290862569321978468622482"
sample += "83972241375657056057490261407972968652414535100474"
sample += "82166370484403199890008895243450658541227588666881"
sample += "16427171479924442928230863465674813919123162824586"
sample += "17866458359124566529476545682848912883142607690042"
sample += "24219022671055626321111109370544217506941658960408"
sample += "07198403850962455444362981230987879927244284909188"
sample += "84580156166097919133875499200524063689912560717606"
sample += "05886116467109405077541002256983155200055935729725"
sample += "71636269561882670428252483600823257530420752963450" 
         
sample_length = sample.length

# 1. selects a selection of a string sample based off a specified range
def sample_select(sample, range)
  sample[range[0]..range[1]]
end

# 2. splits the string into an array of single characters
def split_sample(sample)
  sample.split('')
end

# 3. multiplies an array of numbers
def multiply(numbers)
  product = 1
  numbers.each {|number| product *= number.to_i}
  product
end


# cleaned up - see commit 268c2356bff29f6c5a75dc78891c1b86f4b1b595


# splits a string sample by a specified character (0 in this case)
def split_by(sample, target)
  sample.split("#{target}")
end

# compares a string sample to the length needed and removes strings that measure short
def remove_samples_by_length(samples, length)
  result = []
  samples.each {|n| n.length >= length ? result << n : next}
  result
end

# compare a individual numbers (string)
def sample_compare(x, y)
  x, y = x.to_i, y.to_i
  y > x ? x = y : return
end


# checks a single sample for the highest number set
def find_highest(sample, length)
  start, result = 0, 0
  count, sample_length = length-1, sample.length
  sample_return = ""
  while count < sample_length
    numbers = sample[start..count]
    sample_select = split_sample(numbers)
    comparison = multiply(sample_select)
    if comparison > result
      result = comparison 
      sample_return = numbers
    end
    start += 1
    count += 1
  end
  [result, sample_return]
end




# TEST FUNCTION: see pattern of all combinations
def possibilities
  range = (1..9)
  result = []
  range.each do |number|
    range.each do |n|
      range.each do |x|
        range.each do |y|
          result << [number * n * x * y, "#{number}#{n}#{x}#{y}"]
        end
      end
    end
  end
  result.sort {|x,y| x[0] <=> y[0]}
end

# TEST: split_by
#p split_by(sample, 0)

#sample = split_by(sample, 0)

# TEST: remove_samples_by_length
'''
p "sample 1"
p remove_samples_by_length(sample, 4)
p "sample 2"
p remove_samples_by_length(sample, 13)
'''

#sample = remove_samples_by_length(sample, 4)

# TEST: possibilites
'''
test_set = possibilities
test_set.each {|x| p x}
'''

# TEST: sample_compare
#p sample_compare(1234, 9999)


# TEST: find_highest
'''
test_string = "123456789"
p find_highest(test_string, 4)
'''


# TESTING
def alpha (sample)
  samples = split_by(sample, 0)
  samples = remove_samples_by_length(samples, 4)
  result = []
  samples.each do |sample|
    result << find_highest(sample, 4)
  end
  result.sort {|x,y| x[0] <=> y[0]}[-1]
end

# found the highest 4 digit number product
# p alpha(sample)

def beta (sample)
  samples = split_by(sample, 0)
  samples = remove_samples_by_length(samples, 13)
  result = []
  samples.each do |sample|
    result << find_highest(sample, 13)
  end
  result.sort {|x,y| x[0] <=> y[0]}[-1]
end

# May prove wrong but I will move on and return to proof each of these (if needed)
answer = beta(sample)
p answer