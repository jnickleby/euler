'''
A palindromic number reads the same both ways.
The largest palindrome made from the product of two 2-digit numbers appears as 9009 = 91 × 99.

Find the largest palindrome made from the product of two 3-digit numbers.
'''


# check a number for palindromic properties
def palindrome?(number)
  number = number.to_s
  n_length = number.length
  result = []

  (n_length/2).to_i.times do |n|
    x = n.to_i - 1
    number[x] == number[-1 * n.to_i] ? result << true : result << false
  end

  result
end


# check if all items prove true
def truth_serum(bools)
  admin = true
  bools.each {|bool| bool == false ? admin = false : next}
  admin
end


# find palindromes in a range
def palindromes(start, finish)
  result = []
  for n in (start..finish)
    admin = palindrome?(n)
    admin = truth_serum(admin)
    admin ? result << n : next
  end
  result
end



# TEST: palindrome?
'''
p palindrome?(9009)
p palindrome?(1371731)
'''

# TEST: truth_serum
'''
first = palindrome?(9009)
second = palindrome?(1371731)
third = palindrome?(13471731)

p truth_serum(first)
p truth_serum(second)
p truth_serum(third)
'''

# TEST: palindromes
'''
p palindromes(10,99)
p palindromes(100,199)
p palindromes(200,299)
'''

# PASSED

# final solve
data_set = palindromes(100, 999)
answer = data_set[-1]
p answer