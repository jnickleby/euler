require './ex_1.rb'

'''
By listing the first six prime numbers: 2, 3, 5, 7, 11, and 13, we can see that the 6th prime equals 13.

What represents the 10,001st prime number? -- side note: 10,001st or 10,001th?
'''

# Checks if number represents a prime
def prime_checker(number)
  admin = true
  range = (2..number-1)
  range.each {|n| number % n == 0 ? admin = false : next}
  admin
end

# Iterates through a number range until it reaches the specified number
def prime_runner(ceiling)
  count = 0
  position = 1
  while count < ceiling
    admin = prime_checker(position)
    if admin
      count += 1
    end
    position += 1
  end
  position - 1
end


# TEST: prime_checker
'''
p prime_checker(13)
p prime_checker(21)
'''

# TEST: prime_runner
'''
p prime_runner(6)
p prime_runner(13)
p prime_runner(100)
p prime_runner(1000)
# -- optimize -- p prime_runner(10000)
'''

# WARNING - Takes a long time
answer = prime_runner(10001)
p answer