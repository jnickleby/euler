'''
A Pythagorean triplet has a set of three natural numbers, a < b < c, for which,
a^2 + b^2 = c^2

For example, 3^2 + 4^2 = 9 + 16 = 25 = 5^2.

There exists exactly one Pythagorean triplet for which a + b + c = 1000.
Find the product abc.
'''


def find_c (a, b)
  sum = a**2 + b**2
  c = b + 1
  result = 0
  while result == 0
    c**2 == sum ? result = c : c += 1
    break if c > sum
  end
  result
end


def find_triplets (range)
  a, b = range[0], range[0]+1
  result = []
  range[1].times do |n|
    (range[1]-1).times do |x|
      focus = find_c(n,x)
      focus != 0 ? result << focus.to_s + "#{n}^2 + #{x}^2" : next
    end
  end
  result
end


# TEST
# find_c
'''
p find_c(3,4)
p find_c(3,5)
'''


p find_triplets([1, 10])