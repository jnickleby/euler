require "./ex_1.rb"

'''
The sum of the squares of the first ten natural numbers is,
1^2 + 2^2 + ... + 10^2 = 385

The square of the sum of the first ten natural numbers is,
(1 + 2 + ... + 10)^2 = 55^2 = 3025

Hence the difference between the sum of the squares of the first ten natural numbers and the square of the sum is 3025 − 385 = 2640.

Find the difference between the sum of the squares of the first one hundred natural numbers and the square of the sum.
'''

# generate number range
def generate_numbers(max)
  (1..max) 
end

# sums the squares of a number set
def sum_of_squares(numbers)
  numbers.collect {|number| number**2}  
end


# TEST: first part
'''
numbers = generate_numbers(10)
squared_numbers = sum_of_squares(numbers)
p squared_numbers
sum_of_squares = add(squared_numbers)
p sum_of_squares
square_of_sum = add(numbers)**2
p square_of_sum
p square_of_sum - sum_of_squares
'''

# final solute
numbers = generate_numbers(100)
squared_numbers = sum_of_squares(numbers)
sum_of_squares = add(squared_numbers)
p sum_of_squares
square_of_sum = add(numbers)**2
p square_of_sum
answer = square_of_sum - sum_of_squares
p answer