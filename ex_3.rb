'''
The prime factors of 13195 equal 5, 7, 13, and 29.

What represents the largest prime factor of the number 600851475143 ?
'''


# finds the prime factor of 13195
def factorize(number)
  number, count, result = number, 2, []
  until number == 1
    equalizer = number % count

    if equalizer == 0
      result << count
      puts "Number: #{count} = Total: #{number}"
      number = number / count
    end

    count += 1
  end
  result
end

# iterate through the first data set and reduce the total
def reduce_reuse(numbers, target)
  total = target
  result = []
  numbers.each do |number|
    equalizer = total % number
    if equalizer == 0
      total = total / number
      puts "Number: #{number} = Total: #{total}"
      result << number
    end
  end
  result
end




# TEST: factorize
'''
p factorize(13195)
'''

# TEST: reduce_reuse
'''
target = 13195
numbers = factorize(target)
p reduce_reuse(numbers, target)

p target = 299297458
p numbers = factorize(target)
p reduce_reuse(numbers, target)
'''

# final test
'''
p target = 600851475143
p numbers = factorize(target)
p reduce_reuse(numbers, target)
'''

# FAILED

# TEST: factorize (restructured)
'''
target = 13195
p numbers = factorize(target)

p target = 299297458
p numbers = factorize(target)
'''

# final test
target = 600851475143
numbers = factorize(target)

# PASSED

# ERROR: Other way took to much time to process - had to iterate through every number

answer = numbers[-1]
p answer