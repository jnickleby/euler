require "./ex_1.rb"

'''
Each new term in the Fibonacci sequence generates by adding the previous two terms.
By starting with 1 and 2, the first 10 terms will equal:

1, 2, 3, 5, 8, 13, 21, 34, 55, 89, ...

By considering the terms in the Fibonacci sequence whose values do not exceed four million, find the sum of the
even-valued terms.
'''


# creates a fibonacci number set
def fibonacci_step(steps)
  x, y = 1, 1
  result = []
  steps.times do
    result << y
    x, y = y, x + y
  end
  result
end


# creates a fibonacci number set until it reaches a specified number
def fibonacci(max)
  x, y = 1, 1
  result = []
  until y >= max
    result << y
    x, y = y, x + y
  end
  result
end


# filters out the even numbers
def filter_evens(numbers)
  result = []
  numbers.each do |number|
    if number % 2 == 0
        result << number
        p "#{number} divides evenly by 2"
    end
  end
  result
end


# TEST: fibonacci_step
'''
p fibonacci_step(10)
'''

# TEST: fibonacci
'''
p fibonacci(4000000)
'''

# TEST: filter_evens
'''
numbers = fibonacci(4000000)
p filter_evens(numbers)
'''

# Final solve
numbers = fibonacci(4000000)
numbers = filter_evens(numbers)
answer = add(numbers)
p answer