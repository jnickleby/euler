'''
2520 equals the smallest number that 1 to 10 can divide without any remainder.

What equals the smallest positive number that the numbers from 1 to 20 can evenly divide?
'''


# divides a number by a range
def checker(max, target)
  numbers = (1..max)
  admin = true
  numbers.each {|number| target % number != 0 ? admin = false : next;}
  admin
end

# iterates through numbers and checks to see if each number up to the max can divide the number evenly
def number_run(max, ceiling)
  admin = false
  count = max
  while admin == false
    count += 1
    admin = checker(max, count)
    if count == ceiling
      count = "No matches"
      break
    end
  end
  count
end


# 1. TEST: checker
'''
p checker(10, 2520)
p checker(10, 2521)
'''

# 2. TEST: number_run
'''
p number_run(10, 2520)
'''

#p number_run(10, 4000000)
#answer =  number_run(20, 4000000000)
#p answer


# optimization

def multi_run
  result = []
  test_set = (1..16)
  test_set.each {|n| result << number_run(n, 4000000000000)}
  result
end

p multi_run

# Deciphered rules
# greater than 10 must end in a 0 -- check later