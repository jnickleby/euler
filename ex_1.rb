'''
If we list all the natural numbers below 10 that equal multiples of 3 or 5, we get 3, 5, 6 and 9.
The sum of these multiples equals 23.

Find the sum of all the multiples of 3 or 5 below 1000.
'''

# collects the range based off the maximum (inclusive)
def list_numbers(max)
  agg = [*1..max]
end

# compares number array with a chosen number, returns numbers equally divisible by the chosen number
def compare_collect(numbers, choice)
  result = []
  numbers.each {|number| (number % choice == 0) ? result << number : next}
  result
end

# adds a number array together
def add(numbers)
  sum = 0
  numbers.each {|number| sum += number}
  sum
end

# takes in any list of numbers and a maximum and adds all the multiples together
def multiples_of(numbers, max)
  pool = list_numbers(max)
  result = numbers.collect {|number| compare_collect(pool, number)}
  total = 0
  result.each {|set| total += add(set)}
  total
end

# TEST: list_numbers
'''
p list_numbers(10)
'''

# TEST: compare_collect
'''
numbers = list_numbers(10)
p compare_collect(numbers, 3)
'''

# TEST: add
'''
numbers = list_numbers(10)
numbers = compare_collect(numbers, 3)
p add(numbers)
'''

# TEST: multiples_of
'''
p multiples_of([3,5], 10)
p multiples_of([3,5], 100)
p multiples_of([3,5,7], 100)
'''

# PASSED

'''
numbers = [3, 5]
answer = multiples_of(numbers, 1000)
p answer
'''